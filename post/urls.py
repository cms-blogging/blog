from django.urls import path, include
from rest_framework import routers

from post.views import CategoryViewSet, PostViewSet

router = routers.DefaultRouter()
router.register(r'category', CategoryViewSet)
router.register(r'post', PostViewSet, 'post')

urlpatterns = [
    path('', include(router.urls)),
]