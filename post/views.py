from django.shortcuts import render
from rest_framework import viewsets
from post.PostSerializer import CategorySerializer, PostSerializer
from rest_framework.permissions import IsAuthenticated
from rest_framework_simplejwt.authentication import JWTAuthentication
from django.contrib.auth.models import User



from post.models import Category, Post

# Create your views here.

class DynamicDepthViewSet(viewsets.ModelViewSet):
    def get_serializer_context(self):
        context = super().get_serializer_context()
        depth = 0
        try:
            depth = int(self.request.query_params.get('depth', 0))
        except ValueError:
            pass # Ignore non-numeric parameters and keep default 0 depth
        
        context['depth'] = depth

        return context


class CategoryViewSet(viewsets.ModelViewSet):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer

class PostViewSet(DynamicDepthViewSet):
    queryset = Post.objects.all()
    serializer_class = PostSerializer

    def get_queryset(self):
        if str(self.request.user) != 'AnonymousUser':
            is_superuser = User.objects.filter(username=self.request.user).values_list('is_superuser', flat=True).first()
            if is_superuser:
                return super(PostViewSet, self).get_queryset()
            else:
                qs = super(PostViewSet, self).get_queryset()
                qs = qs.filter(user=self.request.user)
                return qs
        else:
            posts = Post.objects.filter(status = True)
            return posts




