from dataclasses import fields
from rest_framework import serializers
from django.contrib.auth.models import User


from post.models import Category, Post

class DynamicDepthSerializer(serializers.ModelSerializer):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.Meta.depth = self.context.get('depth', 0)

class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = '__all__'

class UserSerializer(DynamicDepthSerializer):
    class Meta:
        model = User
        exclude = ['password',]

class PostSerializer(DynamicDepthSerializer):
    class Meta:
        model = Post
        fields = '__all__'
        
    