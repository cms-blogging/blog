from django.contrib import admin

from post.models import Category, Post, PostUpdate

# admin.site.register(PostStatus)
admin.site.register(Category)
admin.site.register(Post)
admin.site.register(PostUpdate)
